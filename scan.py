#!/usr/bin/env python3

# Application token needs read:follows and write:libraries

import logging
import sys

import utils

from funkwhale_api import FunkwhaleAPI, FunkwhaleAPIBadStatusCode


def main():
    instance_url = utils.get_instance_url()
    token = utils.get_token()
    api = FunkwhaleAPI(instance_url, token)

    print(f"Retrieving followed libraries... ", end="")

    libraries = [e["library"] for e in api.get_federated_libraries()["results"]]
    print(f"{len(libraries)} libraries")

    for lib in libraries:
        print(f"Launching scan of {lib}...", end="")
        ret = api.scan_library(lib)
        print(f" {ret['status']}")


if __name__ == "__main__":
    main()
