# Funkwhale playlist tools

There are three scripts on this repository:

### Semi-automatic playlist import

This script permits to generate playlists on its Funkwhale account from a file looking like this:

```markdown
# Playslist title
Title1 - Artist1
Title2 - Artist2
Title3 - Artist3
...
```

### Playlist export to ZIP

This script permits to export visible playlists from its Funkwhale account to a ZIP archive.

### Playlist export to TXT

This script permits to export visible playlists from its Funkwhale account to file with the format of the first script.

### Playlist and Album download

This script permits to download scripts and albums.

## Dependencies installation

### Regular linux

With a standard linux box, intialise a python virtual env:

```sh
python -m venv env
```

Then enter the virtual env:

```sh
source env/bin/activate
```

Then install the dependencies:

```sh
python -m pip install -r requirements.txt
```

Exit with:

```sh
deactivate
```

### NixOS

Enter the environment:

```sh
nix-shell
```

Exit:

```sh
exit
```

## Environment variables

- a file `secrets/instance_url` containing for example `https://music.ppom.me`
- a file `secrets/token` containing for example `concomber-insalata-pomodoro-pottermore-friend`. You can generate a token by creating a new application in your Funkwhale account settings
- a file `secrets/save_path` containing the path to save ZIP files from the first export tool.
- a file `secrets/txt_path` containing the path to save TXT files from the second export tool.

## Execution

```sh
usage: import-from-txt.py [-h] [-i] [-t THRESHOLD] [-f] [--allow-duplicates] [--try-hard] [-l LOG_LEVEL] [path]

import a text-formatted playlist into a Funkwhale instance

positional arguments:
  path                  path of the file containing the text-formatted playlist.
                        use - for stdin (disables interactivity)

options:
  -h, --help            show this help message and exit
  -i, --interactive     ask to manually enter a track ID if track not found
  -t THRESHOLD, --threshold THRESHOLD
                        maximal distance 0 <= N <= 100 for two strings to be considered similar
  -f, --force           if an existing playlist created by the current user with the same name is found, rewrite it
  --allow-duplicates    if an existing playlist created by another user with the same name is found, create a playlist anyway
  --try-hard            split terms of search to maximize chances of findings in special cases
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        configure the logging level (DEBUG, INFO, WARNING, or ERROR)

playlist should be formatted as follow:

# Playlist name
Title1 - Artist1
Title2 - Artist2

read the module docstring for extended documentation.
```

```sh
usage: export-to-zip.py [-h] [-n] [-l LOG_LEVEL] [save_path] playlist_id [playlist_id ...]

export Funkwhale playlists to ZIP files

positional arguments:
  save_path             output path (default : secret `save_path`)
  playlist_id

options:
  -h, --help            show this help message and exit
  -n, --normalize       Normalize to max amplitude
  -l LOG_LEVEL, --log-level LOG_LEVEL
                        configure the logging level (DEBUG, INFO, WARNING, or ERROR)
```

```sh
python export-to-txt.py <ID> [ ADDITIONAL_IDS... ]
```

```sh
python download.py albums|playlists <ID> [ ADDITIONAL_IDS... ]
```

Additionnal documentation can be found in [get-random-album.py](./get-random-album.py).
```sh
# Prints a random album link
python get-random-album.py [all,me,subscribed,...]
```