#!/usr/bin/env python3

"""
Import a text-formatted playlist into a Funkwhale instance.


Funkwhale is a federated platform for managing and listening musical libraries.

Users can follow libraries located on other instances and listen to remote content.
However, at the time of writing, playlists are not federated. It means that even
if you have access to the content of a remote user, you cannot listen to their playlists.

This script, in conjunction to [export-to-txt.py](./export-to-txt.py), makes easy
to mirror a remote playlist, in a two-step process:
* User A generates a text-formatted playlist from their Funkwhale instance ← the other script
* User B uses the search feature of their instance to create a mirror playlist ← this script

The search feature is the only way to find the tracks, because there is not GUID for tracks
across the federation. Plus, the track list could come from another source.

Note if that the federation contains duplicates of the tracks, maybe you won't get a
reference to the track hosted by user A. You could get a reference to the same track,
hosted by user C. If user C is unaware of your playlist game and close their instance,
your playlist won't work anymore. But you just have to launch that script again.
That's the way federation is supposed to work, right ?

Unfortunately, "importing" a playlist through search is not that simple, because:
1. Funkwhale search is broken in edge cases.
2. You may want to import a playlist from an external source, with slight artist/title differences.

In both cases, searching for the title could yield multiple results
and the script would have to choose. In an attempt to get reliable result , the logic is as follows:

┌──────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                   Playlist management│
│                                ┌──────────────────────┐         ┌───────────────────────┐            │
│         ┌───────────────┐      │ Playlist(s) belonging│  no     │ Playlist belonging to │            │
│         │     START     ├─────►│ to the user with     ├────────►│ another user with the │            │
│         └───────────────┘      │ the same name ?      │         │      same name?       │            │
│                                └───────────┬──────────┘         └──────────┬───────────┬┘            │
│                                            │                               │           │             │
│                                       yes  │                               │           │             │
│                                            │                               │           │             │
│                                            ▼                               │           │             │
│         ┌────────────────┐  no  ┌──────────────────────┐                   │           │             │
│         │ Force override │◄─────┤  Multiple playlists? │                   │ yes       │             │
│         │    mode?       │      └──────────┬───────────┘                   │           │             │
│         └────┬──────┬────┘                 │                               │           │ no          │
│              │      │                yes   │                               │           │             │
│              │      │                      │                               │           │             │
│              │      │                      ▼                               ▼           │             │
│         yes  │      │            ┌─────────────────────┐ yes    ┌────────────────────┐ │             │
│              │      │            │Skip duplicates mode?├───────►│ Cancel the import  │ │             │
│              │      │            └───────────────────┬─┘        └────────────────────┘ │             │
│              │      │                                │                 ▲               │             │
│              │      │                                │                 │               │             │
│              │      └────────────────────────────────┼─────────────────┘               │             │
│              │                             no        │                                 │             │
│              │                                       └────────────────────────┐        │             │
│              ▼                                                     no         ▼        ▼             │
│         ┌──────────────────┐         ┌─────────────────────┐               ┌─────────────────────┐   │
│         │Delete its content├────────►│ Iterate over tracks │◄──────────────┤ Create the playlist │   │
│         └──────────────────┘         └─────────┬───────────┘               └─────────────────────┘   │
│                                                │                                                     │
│                                                │                                                     │
└────────────────────────────────────────────────┼─────────────────────────────────────────────────────┘
                                                 │
┌────────────────────────────────────────────────┼─────────────────────────────────────────────────────┐
│                                                │                          Individual track management│
│                                                ▼                                                     │
│                                      ┌────────────────────┐                                          │
│                                      │Get all artists with│                                          │
│                                      │similarity over the │                                          │
│                                      │     threshold      │                                          │
│                                      └─────────┬──────────┘                                          │
│                                                │                                                     │
│                                                ▼                                                     │
│                                     ┌───────────────────────┐                                        │
│                                     │  Search the track in  │                                        │
│                                     │  all similar artists' │                                        │
│                                     │      discography      │                                        │
│                                     └───────────┬───────────┘                                        │
│                                                 │                                                    │
│                                                 ▼                                                    │
│      ┌────────────────────┐   yes    ┌─────────────────────┐                                         │
│      │   Add title to     │◄─────────┤Is there a track with│                                         │
│      │   the playlist     │          │  similarity above   │                                         │
│      └────────────────────┘          │   the threshold?    │                                         │
│                  ▲                   └──────────┬──────────┘                                         │
│                  │                              │                                                    │
│                  │                              │ no                                                 │
│                  │                              ▼                                                    │
│                  │                    ┌──────────────────┐                                           │
│                  │                    │  Interactive     ├─────────────────┐                         │
│                  │ yes                │     mode?        │                 │                         │
│                  │                    └─────────┬────────┘                 │                         │
│                  │                              │                      no  │                         │
│                  │                              │ yes                      │                         │
│                  │                              │                          │                         │
│                  │                              ▼                          │                         │
│                  │                   ┌────────────────────┐                ▼                         │
│                  │                   │  Manually enter    │  no   ┌────────────────────┐             │
│                  └───────────────────┤ a local track ID?  ├──────►│  Ignore the title  │             │
│                                      └────────────────────┘       └────────────────────┘             │
│                                                                                                      │
└──────────────────────────────────────────────────────────────────────────────────────────────────────┘

In the diagram above:
*Search* means searching through the Funkwhale API, which tries to return similar results.
Note that searching is done by splitting artist and title with space if no result is returned
the first time, as the Funwkhale search is quite strict.
*Similarity* is to be understood in terms of threshold and Levenshtein ratio.

This [0, 100] ratio captures the "difference" between two strings (it is a normalized version
of the Levenshtein distance, which is comprised between 0 and the length of the longest string).
Candidate with *maximum similarity* simply means the candidate with the lowest ratio.

The default threshold is chosen by rule of thumb but should be ok.

So there is two levels of filtering: through the Funkwhale search, which is sometimes
broken, and then through Levensthein ratios. This second level is especially useful
when multiple artists have been found similar with the original. However, the subtility
there is that multiple actors could have the "same" artist (i.e. same name) but with
very different tracks. Thus, we need to merge all search results of the original track
restricted to each one of these artists, and then to find the most similar track.

Also, the rationale behind deleting a playlist content if it already exists is that:
* Updating the playlist would require specific code to find differences, and
  annoying logic in presence of duplicate tracks in the federation;
* Inserting a track requires two API calls;
* Reimporting can be done specifically to update the track IDs that no longer work.
"""

import argparse
import fileinput
import logging
import sys

from textwrap import dedent

import utils

from funkwhale_api import FunkwhaleAPI, FunkwhaleAPIBadStatusCode, FunkwhaleMatcher

class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawTextHelpFormatter):
    pass

MIN_THRESHOLD = 0
MAX_THRESHOLD = 100


def build_parser() -> argparse.ArgumentParser:
    """Returns a parser for the script with help built in."""
    parser = argparse.ArgumentParser(
        description="import a text-formatted playlist into a Funkwhale instance",
        epilog=dedent(
            """
        playlist should be formatted as follow:

        # Playlist name
        Title1 - Artist1
        Title2 - Artist2

        read the module docstring for extended documentation.
        """
        ),
        formatter_class=CustomFormatter,
    )
    parser.add_argument(
        "path",
        # Optional positional argument: either specified or default stdin so playlist can be piped into this program
        nargs="?",
        help="path of the file containing the text-formatted playlist.\nuse - for stdin (disables interactivity)",
        default="-",
    )
    parser.add_argument(
        "-i",
        "--interactive",
        action="store_true",
        help="ask to manually enter a track ID if track not found",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="do everything, except creating the actual playlist",
    )
    parser.add_argument(
        "-t",
        "--threshold",
        default=20,
        help=f"maximal distance {MIN_THRESHOLD} <= N <= {MAX_THRESHOLD} for two strings to be considered similar",
    )
    parser.add_argument(
        "-f",
        "--force",
        action="store_true",
        help="if an existing playlist created by the current user with the same name is found, rewrite it",
    )
    parser.add_argument(
        "--allow-duplicates",
        action="store_true",
        help="if an existing playlist created by another user with the same name is found, create a playlist anyway",
    )
    parser.add_argument(
        "--try-hard",
        action="store_true",
        help="split terms of search to maximize chances of findings in special cases",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="INFO",
        # Trick to convert string to numerical log level
        type=lambda x: getattr(logging, x.upper()),
        help="configure the logging level (DEBUG, INFO, WARNING, or ERROR)",
    )
    return parser


def main():
    """Main logic to import the text-formatted playlist into a user-defined Funkwhale instance."""
    # Get and validate arguments from the CLI
    parser = build_parser()
    args = parser.parse_args()
    if not 0 <= args.threshold <= 100:
        parser.print_help()
        raise ValueError(
            f"Threshold should be between {MIN_THRESHOLD} and {MAX_THRESHOLD}."
        )
    if args.path == "-":
        args.interactive = False

    # Configure logging
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s:%(levelname)-7s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(args.log_level)

    # Authenticate to the instance and configure utility matcher
    instance_url = utils.get_instance_url()
    token = utils.get_token()
    api = FunkwhaleAPI(instance_url, token)
    matcher = FunkwhaleMatcher(api, args.threshold, args.try_hard)
    user_id = api.get_me()["id"]

    # Extract information from file
    try:
        logging.debug(f"Parsing file {args.path}...")
        lines = []
        with fileinput.input(files=args.path) as f:
            for line in f:
                lines.append(line)
        playlist_title = utils.read_txt_playlist_name(lines)
        tracks = utils.read_txt_artist_item_content(lines)
    except utils.LocalError:
        logging.exception(f"Error when parsing file {args.path}, aborting.")
        sys.exit(1)
    logging.info(f"Importing playlist {playlist_title}...")

    # Handle playlist operations
    logging.debug("Looking for existing playlist...")
    search_results = api.get_playlists({"q": playlist_title})["results"]
    same_playlists = [p for p in search_results if p["name"] == playlist_title]
    own_same_playlists = [p for p in same_playlists if p["user"]["id"] == user_id]
    if len(own_same_playlists) > 1:
        raise ValueError(
            f'❌ Found multiple existing playlists owned by current user with name "{playlist_title}".'
        )
    # Clear existing owned playlist if any
    if own_same_playlists:
        if not args.force:
            logging.warning(
                f'Found an owned playlist with identical name "{playlist_title}", aborting.'
            )
            sys.exit(0)

        logging.info(
            f'Found an owned playlist with identical name "{playlist_title}", clearing it...'
        )
        playlist_id = own_same_playlists[0]["id"]
        if not args.dry_run:
            api.clear_playlist(playlist_id)
    # If the only playlist found are created by another user, skip it if asked
    elif same_playlists and not args.allow_duplicates:
        logging.warning(
            f'⚠️  Playlist with identical name "{playlist_title}" found but created by another user, aborting.'
        )
        sys.exit(0)
    # Otherwise, create the playlist
    else:
        logging.info(f'Creating new playlist "{playlist_title}"...')
        if not args.dry_run:
            new_playlist = api.create_playlist(
                {"name": playlist_title, "privacy_level": "instance"}
            )
            playlist_id = new_playlist["id"]
        else:
            playlist_id = "fake-id"

    # List of track IDs to add to the playlist
    track_ids = []

    logging.debug("Start to loop over tracks.")
    for track in tracks:
        logging.info(f"Searching for {track}...")
        matching_tracks = matcher.matching_item(track)
        if not matching_tracks:
            # Interactive mode
            if args.interactive:
                logging.info(
                    "Interactive mode enabled: asking for track ID. Enter for skipping."
                )
                # Record manually entered track ID
                track_id = str(input(f"Local track ID for {track} ? "))
                if track_id:
                    track_ids.append(track_id)
                    continue
            logging.warning(
                f"⚠️  Title {track.title} not found with enough confidence and skipped."
            )
            continue
        # Keep the most similar track among candidate
        track_ids.append(matching_tracks[0])

    # At the end, add all tracks at once to the playlist
    logging.debug(f"Track IDs are {track_ids}")

    if len(track_ids) > 0:
        logging.info("Now adding tracks to playlist.")
        try:
            if not args.dry_run:
                api.add_tracks_to_playlist(
                    playlist_id, {"tracks": track_ids, "allow_duplicates": "true"}
                )
        except FunkwhaleAPIBadStatusCode:
            logging.exception("❌ Error while adding tracks to playlist.")
        else:
            logging.info(
                f"Done. Playlist link: {instance_url}/library/playlists/{playlist_id}"
            )

    if len(track_ids) == len(tracks):
        logging.info("✅ All tracks imported")
    elif len(track_ids) < len(tracks):
        logging.warning(f"⚠️  Only {len(track_ids)}/{len(tracks)} tracks imported")
    else:
        logging.error("❌ No track imported")


if __name__ == "__main__":
    main()
