#!/usr/bin/env python3

import argparse
import os
import logging
import tempfile
from zipfile import ZipFile

from utils import *


def main():
    """
    Takes playlist IDs on the command line and save each playlist as 
    a ZIP, optionnaly with normalization per-playlist.
    """
    parser = argparse.ArgumentParser(
        description="export Funkwhale playlists to ZIP files",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "save_path",
        # Optional positional argument: either specified or fall back to secret
        nargs="?",
        help="output path (default : secret `save_path`)",
        default=os.path.expanduser(get_secret("save_path")),
    )
    parser.add_argument(
        "-n",
        "--normalize",
        default=False,
        action='store_true',
        help=f"Normalize to max amplitude",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="INFO",
        # Trick to convert string to numerical log level
        type=lambda x: getattr(logging, x.upper()),
        help="configure the logging level (DEBUG, INFO, WARNING, or ERROR)",
    )
    parser.add_argument(
        'playlist_id',
        # At least one playlist id
        nargs='+',
        type=int
    )
    args = parser.parse_args()

    # Configure logging
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s:%(levelname)-7s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(args.log_level)

    if not os.path.exists(args.save_path):
        os.mkdir(args.save_path)

    base_url = get_base_url()
    instance_url = get_instance_url()
    s = get_funkwhale_session()
    if args.normalize:
        from pydub import AudioSegment, effects
        from pydub.utils import mediainfo
        logging.info(f'All tracks will be normalized.')

    for id in args.playlist_id:
        playlist = dict()
        try:
            playlist = s.get(f"{base_url}/playlists/{id}").json()
            name = playlist["name"]
            logging.info(f"Playlist {name} :")
            tracks = s.get(f"{base_url}/playlists/{id}/tracks").json()["results"]
            uuids = {}

            for t in tracks:
                t = t["track"]
                uuids[t["listen_url"]] = (t["title"].replace("/", ""), t["artist"]["name"].replace("/", ""))

            logging.info("Saving files to ZIP...")

            with tempfile.TemporaryDirectory(prefix="funkwhale-export") as directory:
                dirname = slugify(name)
                zip_path = f"{args.save_path}/{dirname}.zip"
                zip_file = ZipFile(zip_path, "w")
                i = 0
                for listen_url, (title, artist) in uuids.items():
                    i = i + 1
                    r = s.get(f"{instance_url}{listen_url}", params={"to": "mp3"})
                    # 02d means that we pad zeroes to have at least 2 digits
                    slug_title = f'{slugify(title)}_{slugify(artist)}'
                    file_name = f"{i:02d}-{slug_title}.mp3"

                    logging.info(f"Writing {file_name}...")
                    file_path = f"{directory}/{file_name}"

                    with (open(file_path, "wb")) as f:
                        f.write(r.content)

                    if args.normalize:
                        sound = AudioSegment.from_file(file_path, 'mp3')
                        normalized_sound = effects.normalize(sound)
                        # Include original metadata
                        normalized_sound.export(file_path, format='mp3', tags=mediainfo(file_path).get('TAG', {}))

                    zip_file.write(
                        file_path, dirname + "/" + os.path.basename(file_path)
                    )

            logging.info(f"Playlist saved as {zip_path}.")
        except Exception as e:
            logging.error(f"Error on playlist id {id}: {e}")
            if not "name" in playlist and "detail" in playlist:
                logging.error(f"Error details: {playlist['detail']}")


if __name__ == "__main__":
    main()
