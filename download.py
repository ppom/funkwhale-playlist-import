#!/usr/bin/env python3

import os
import sys

from utils import *


def main():
    args_ids = []

    arg_type = sys.argv[1]
    if not arg_type in ["albums", "playlists"]:
        print(
            "Error: the first argument must be one of: albums, playlists",
            file=sys.stderr,
        )
        sys.exit(1)

    singular = arg_type[:-1]

    try:
        args_ids = [int(arg) for arg in sys.argv[2:]]
    except ValueError:
        print("Error: an id is not a number", file=sys.stderr)
        sys.exit(1)

    try:
        save_path = os.path.expanduser(get_secret("save_path"))

        if not os.path.exists(save_path):
            os.mkdir(save_path)

        base_url = get_base_url()
        instance_url = get_instance_url()
        s = get_funkwhale_session()

    except LocalError as e:
        print(f"Error: {e.what}", file=sys.stderr)
        sys.exit(1)

    for id in args_ids:
        playlist = dict()
        try:
            item = s.get(f"{base_url}/{arg_type}/{id}").json()
            if arg_type == "playlists":
                name = item["name"]
                tracks = s.get(f"{base_url}/playlists/{id}/tracks").json()["results"]
            else:
                name = item["title"]
                album_params = {
                    "ordering": "disc_number,position",
                    "album": id,
                    "page_size": 50,
                    "page": 1,
                    "include_channels": True,
                }
                tracks = s.get(f"{base_url}/tracks", params=album_params).json()[
                    "results"
                ]

            print("---------------")
            print(f"{singular}: {name} ")

            uuids = {}

            for t in tracks:
                if arg_type == "playlists":
                    t = t["track"]
                uuids[t["listen_url"]] = t["title"].replace("/", "")

            dirname = os.path.join(save_path, slugify(name))
            os.mkdir(dirname)
            i = 0
            for k, v in uuids.items():
                i = i + 1
                r = s.get(f"{instance_url}{k}", params={"to": "mp3"})
                # 02d means that we pad zeroes to have at least 2 digits
                slug_title = slugify(v)
                file_name = f"{i:02d}-{slug_title}.mp3"

                print(f"\tSaving {v}...")
                file_path = f"{dirname}/{file_name}"

                with (open(file_path, "wb")) as f:
                    f.write(r.content)

            print(f"Tracks downloaded in {dirname}.")
        except Exception as e:
            print(f"Error on {singular} id {id}: {e}", file=sys.stderr)
            if not "name" in playlist and "detail" in playlist:
                print(f"Error details: {playlist['detail']}", file=sys.stderr)


if __name__ == "__main__":
    main()
