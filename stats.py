#!/usr/bin/env python3

"""
Produces stats yay

# usage: python stats.py jean
# output:

Jean has made xx playlists.
Jean's playlists contain between xx and xx tracks, average xx tracks, for a total of xx tracks.
xx tracks have been included multiple times. Here are those favorites, check it out!

💜💜💜 times
Call Me Maybe - Carly Rae Jepsen

💜💜 times
Fumer tiser baiser - Lune de fiel

Favorite artists:

💜💜💜 times
Carly Rae Jepsen

💜💜 times
Lune de fiel

But Jean is not alone here 😉
Jean's playlists have songs that others have included in their playlists as well:

💞 Tom
You make my dreams come true - Daryl Hall & John Oates 
"""

import fileinput
import re
import sys
from pathlib import Path

import utils

def user_upper(user: str) -> str:
    return f"{user[0].upper()}{user[1:]}"


def main():

    txt_path = Path(utils.get_secret("txt_path")).expanduser()
    user = sys.argv[1]

    files = Path(txt_path).glob(f"{user}-*")

    if not files:
        print(f"No file with a name like '{txt_path}/{user}-*'")
        sys.exit(1)

    files = Path(txt_path).iterdir()

    #           ↓ Track      User ↓    ↓ SongCount
    songs: dict[utils.ArtistItem, dict[str, int]] = {}

    playlist_sizes = []

    for file in files:
        try:
            file_user = re.match("^([^-]+)-.*", file.name)[1]
        except:
            print(f"Error on file {file}: Could not deduce user name")
            continue
        
        lines = []
        with fileinput.input(files=str(file)) as f:
            for line in f:
                lines.append(line)
        file_songs = utils.read_txt_artist_item_content(lines)

        # Save playlist length
        if file_user == user:
            playlist_sizes.append(len(file_songs))

        for song in file_songs:
            if not song in songs:
                songs[song] = {}
                songs[song][file_user] = 1
            elif not file_user in songs[song]:
                songs[song][file_user] = 1
            else:
                songs[song][file_user] += 1

    # Only keep songs which have this user
    songs = { k:v for (k,v) in songs.items() if user in v }

    User = user_upper(user)

    playlist_number = len(playlist_sizes)
    playlist_min = min(playlist_sizes)
    playlist_max = max(playlist_sizes)
    playlist_total = sum(playlist_sizes)
    playlist_average = playlist_total / playlist_number

    duplicates = { k:v[user] for (k,v) in songs.items() if v[user] > 1 }
    number_of_duplicates = len(duplicates)
    max_duplicate = max(duplicates.values())

    print(f"{User} has made {playlist_number} playlists.")
    print(f"{User}'s playlists contain between {playlist_min} and {playlist_max} tracks, average {playlist_average:.1f} tracks, for a total of {playlist_total} tracks.")
    print(f"{number_of_duplicates} tracks have been included multiple times, up to {max_duplicate} times. Here are those favorites, check it out!")
    print()

    for x_times in range(max_duplicate, 1, -1):
        for i in range(x_times):
            print("💜", end="")

        print(" times")
        
        x_times_songs = [ k for (k,v) in duplicates.items() if v == x_times ]

        for song in x_times_songs:
            print(repr(song))

        print()

    # Artists part

    artists = {}
    for (k,v) in songs.items():
        if not k.artist in artists:
            artists[k.artist] = 1
        else:
            artists[k.artist] += 1

    max_artists = max(artists.values())

    if max_artists > 1:
        print("Favorite artists:")
        print()

        for x_times in range(max_artists, 1, -1):
            print("with ", end="")

            for i in range(x_times):
                print("💜", end="")

            print(" distinct tracks")
            
            x_times_artists = [ k for (k,v) in artists.items() if v == x_times ]

            for artist in x_times_artists:
                print(artist)

            print()

    # Friends part

    friends = {}
    for (k, v) in songs.items():
        if user in v and len(v) > 1:
            for u in set(v.keys()) - set([user]):
                if not u in friends:
                    friends[u] = [k]
                else:
                    friends[u].append(k)

    if friends:
        print(f"But {User} is not alone here 😉")
        print(f"{User}'s playlists have songs that others have included in their playlists as well:")
        print()

        for (k,v) in friends.items():
            print(f"💞 {user_upper(k)}")

            for song in v:
                print(repr(song))

            print()

if __name__ == "__main__":
    main()
