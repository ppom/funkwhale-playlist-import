#!/usr/bin/env python3

import sys

from utils import *

def main():
    """
    Prints the link to a random album of your instance.
    Useful if you want to discover something.

    Scope can be :
    * `all` (or do not specify the property to disable scope filtering)
    * `me` to retrieve content relative to the current user
    * `subscribed` to retrieve content in libraries you follow
    * `actor:alice@example.com` to retrieve content relative to the account `alice@example.com
    * `domain`:example.com` to retrieve content relative to the domain `example.com

    You can specify multiple coma-separated scopes.

    Cf : https://docs.funkwhale.audio/swagger/#/Library%20and%20metadata/get_api_v1_albums_

    Examples :
    $ ./get-random-album.py
    No scope specified, choosing among all albums.
    https://music.chosto.me/library/albums/8252

    $ ./get-random-album.py me
    https://music.chosto.me/library/albums/8699

    $ ./get-random-album.py 'actor:aranelle@music.chosto.me'
    https://music.chosto.me/library/albums/8712
    """
    if len(sys.argv) > 1:
        scope = sys.argv[1]
    else:
        scope = "all"
        print("No scope specified, choosing among all albums.")

    base_url = get_base_url()
    instance_url = get_instance_url()
    s = get_funkwhale_session()
    params = {
        "scope": scope,
        "page": 1,
        "page_size": 1,
        "ordering": "random",
        "playable": "true",
        "include_channels": "true",
        "content_category": "music",
    }
    results = s.get(f"{base_url}/albums", params=params).json()
    id = results["results"][0]["id"]
    print(f"{instance_url}/library/albums/{id}")


if __name__ == "__main__":
    main()
