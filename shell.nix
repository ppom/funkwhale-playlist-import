with import <nixpkgs> {};

let
  python = python3;
  pythonPackages = python3Packages;
in
pkgs.mkShell {
  name = "pip-env";
  buildInputs = with pythonPackages; [
    requests
    types-requests
    virtualenv
    rapidfuzz
    black
    mypy
  ];
  src = null;
  shellHook = ''
    # Allow the use of wheels.
    SOURCE_DATE_EPOCH=$(date +%s)

    VENV=env
    if test ! -d $VENV; then
      virtualenv $VENV
    fi
    source ./$VENV/bin/activate

   export PYTHONPATH=`pwd`/$VENV/${python.sitePackages}/:$PYTHONPATH
  '';
}

