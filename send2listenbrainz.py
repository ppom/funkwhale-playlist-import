"""
Import all your listen history from Funkwhale to ListenBrainz

Then let the Funkwhale ListenBrainz plugin handle the rest!
(It doesn't retroactively import your history, that's why I wrote this script)

PSQL Funkwhale query this script reads from:
```sql
\copy (SELECT
t.title track_name,
a.title release_name,
ar.name artist_name,

t.mbid recording_mbid,
a.mbid release_group_mbid,
ar.mbid artist_mbids,

EXTRACT(epoch from h.creation_date) listened_at,
t.position track_number

FROM history_listening h
INNER JOIN music_track t ON h.track_id = t.id
INNER JOIN music_album a ON a.id = t.album_id
INNER JOIN music_artist ar ON t.artist_id = ar.id
WHERE user_id = <USER>
ORDER BY h.creation_date)
TO '/tmp/query.tsv' (FORMAT CSV, DELIMITER E'\t');
```
"""

import csv
import sys
import pylistenbrainz

client = pylistenbrainz.ListenBrainz()
client.set_auth_token(input("token: "))

with open('/tmp/query.tsv') as file:
    tsv = csv.reader(file, delimiter="\t")

    exit = False
    cpt = 0
    while not exit:
        listenings = []
        # ListenBrainz limit to batches of 1000 listenings.
        # I chose to run only 100, but you can increase if you want
        for i in range(100):
            line = next(tsv, False)
            if not line:
                exit = True
            else:
                track_name, release_name, artist_name, recording_mbid, release_group_mbid, artist_mbids, listened_at, tracknumber = None, None, None, None, None, None, None, None
                if line[0]:
                    track_name = line[0]
                if line[1]:
                    release_name = line[1]
                if line[2]:
                    artist_name = line[2]
                if line[3]:
                    recording_mbid = line[3]
                if line[4]:
                    release_group_mbid = line[4]
                if line[5]:
                    artist_mbids = [line[5]]
                if line[6]:
                    try:
                        listened_at = line[6].split(".")[0]
                    except e:
                        print("error", e)
                if line[7]:
                    tracknumber = line[7]

                listen = pylistenbrainz.Listen(
                    track_name,
                    artist_name,
                    listened_at=listened_at,
                    release_name=release_name,
                    recording_mbid=recording_mbid,
                    release_group_mbid=release_group_mbid,
                    artist_mbids=artist_mbids,
                    tracknumber=tracknumber,
                )
                listenings.append(listen)
        print("batch", cpt)
        cpt += 1
        client.submit_multiple_listens(listenings)
