// In /manage/library/uploads?q=status%3Aerrored
// When there are a lot of errorred uploads, and you want to delete this logs, for a fresh start:

// Declare this two functions in the console
function clearr() {
  document.body.querySelector("tbody").querySelectorAll("input[type='checkbox']").forEach(c => c.click());
  document.body.querySelector("thead button.ui.button").click();
  setTimeout(() => document.body.querySelector("button.ui.confirm.danger.button").click(), 200);
}

// fans of infinite recursion
function clearrr() { setTimeout(() => { clearr(); clearrr(); }, 3000)}

// Then run the second function:
clearrr()

// It will select all the paginated uploads, click Delete, click on the confirmation box, and go again *ad vitam eternam*
// When it's done, just reload the page to stop the script
