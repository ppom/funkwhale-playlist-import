#!/usr/bin/env python3

import os
import sys

import utils


def main():
    playlist_ids = []

    try:
        playlist_ids = [int(arg) for arg in sys.argv[1:]]
    except ValueError:
        print("Error: a command-line argument is not a number")

    try:
        txt_path = os.path.expanduser(utils.get_secret("txt_path"))

        if not os.path.exists(txt_path):
            os.mkdir(txt_path)

        base_url = utils.get_base_url()
        s = utils.get_funkwhale_session()

    except LocalError as e:
        print(f"Error: {e}")
        sys.exit(1)

    for id in playlist_ids:
        playlist = dict()
        track = dict()
        try:
            playlist = s.get(f"{base_url}/playlists/{id}").json()
            name = playlist["name"]
            tracks = s.get(f"{base_url}/playlists/{id}/tracks").json()["results"]

            filename = utils.slugify(name)
            file_path = f"{txt_path}/{filename}.md"
            with open(file_path, "w+") as file:
                file.write(f"# {name}\n")
                for track in tracks:
                    title = track["track"]["title"]
                    artist = track["track"]["artist"]["name"]
                    file.write(f"{title} - {artist}\n")

            print(f"Playlist {id}'s file saved as {file_path}")
        except Exception as e:
            print(f"Error on playlist id {id}: {e}")
            if not "name" in playlist and "detail" in playlist:
                print(f"Error details: {playlist['detail']}")
            elif not "track" in track and "detail" in track:
                print(f"Error details: {track['detail']}")


if __name__ == "__main__":
    main()
