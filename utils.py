import re
import logging
import requests
import fileinput
import unicodedata

from dataclasses import dataclass
from enum import Enum

TXT_TITLE_PREFIX = "# "
TXT_TRACK_SEPARATOR = " - "

class FunkwhaleArtistItem(Enum):
    """Any funkwhale item associated with an artist."""
    TRACK = 1
    ALBUM = 2

class FunkwhaleError(Exception):
    def __init(self, what: str):
        self.what = what


class LocalError(FunkwhaleError):
    pass


class RemoteError(FunkwhaleError):
    pass


class IllformedTitleException(LocalError):
    """Indicates that the title in the text-formatted playlist is in the wrong format."""


class IllformedTrackException(LocalError):
    """Indicates that a track in the text-formatted playlist is in the wrong format."""


# Tracks are read-only so that Track objects are hashable
# This is mandatory for caching
@dataclass(frozen=True)
class ArtistItem:
    """Simple data object to hold artist and title information"""

    artist: str
    title: str

    def __repr__(self):
        return f"{self.artist} - {self.title}"

def get_secret(name: str) -> str:
    path = f"secrets/{name}"
    try:
        secret = open(path, "r").read().strip()
        if not secret:
            raise LocalError(f"secret {path} is empty")
        return secret
    except LocalError:
        raise
    except:
        raise LocalError(f"{path} doesn't seem to exist")


def get_instance_url() -> str:
    return get_secret("instance_url")


def get_token() -> str:
    return get_secret("token")

def get_lastfm_secret() -> str:
    return get_secret("lastfm_secret")

def get_lastfm_key() -> str:
    return get_secret("lastfm_key")

def get_base_url() -> str:
    instance_url = get_secret("instance_url")
    return f"{instance_url}/api/v1"


def get_funkwhale_session() -> requests.Session:
    token = get_secret("token")
    s = requests.Session()
    s.headers.update({"Authorization": f"Bearer {token}"})
    return s


# Taken from here: https://stackoverflow.com/questions/295135/turn-a-string-into-a-valid-filename
def slugify(name: str, allow_unicode=True):
    if allow_unicode:
        value = unicodedata.normalize("NFKC", name)
    else:
        value = (
            unicodedata.normalize("NFKD", name)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


def read_txt_playlist_name(lines: list[str]) -> str:
    """Returns a playlist name from a text-formatted playlist"""
    candidate_title = lines[0]
    if not candidate_title.startswith(TXT_TITLE_PREFIX):
        raise IllformedTitleException(
            f'Playlist title "{candidate_title}" should start with "{TXT_TITLE_PREFIX}"'
        )
    return candidate_title.split(TXT_TITLE_PREFIX)[1].strip()


def read_txt_artist_item_content(lines: list[str]) -> list[ArtistItem]:
    """Returns a list of artist items extracted from a text-formatted file."""
    items: list[ArtistItem] = []
    # Fileinput handles stdin if path is -
    for line in lines:
        # Will happen when reading a file, but not when reading stdin
        if line.startswith(TXT_TITLE_PREFIX):
            continue
        # Exceptation is that each line is formatted as follows with arbitrary number of spaces:
        # Artist TXT_TRACK_SEPARATOR Title
        line = line.strip()
        line_sep_split = line.split(TXT_TRACK_SEPARATOR)
        if len(line_sep_split) < 2:
            raise IllformedTrackException(f'Cannot parse artist and title "{line}"')
        # Artist name and item title should not contain the separator.
        # However, if it ever would, better to have an heuristic.
        # We assume that the title is more likely to contain an exotic
        # separator than the artist, so if more than two elements are found,
        # the artist is taken from the last element and the title is made of the rest.
        artist = line_sep_split[-1].strip()
        title = TXT_TRACK_SEPARATOR.join(line_sep_split[:-1]).strip()
        if len(line_sep_split) > 2:
            logging.warning(
                f'⚠️  Artist/title separator "{TXT_TRACK_SEPARATOR}" found multiple times in line {line}. Assuming artist name is {artist}.'
            )
        # Funwkhale artists with single quotes are only found when using apostrophes
        title = title.replace("'", "’")
        items.append(ArtistItem(artist, title))
    return items
