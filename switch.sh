#!/usr/bin/env bash

usage() {
    echo "usage: switch.sh alice is bob"
    echo "usage: switch.sh alice is bob for carol"
    echo
    echo "...and Alice will temporarily be named Bob"
    echo
    echo "add 'for name' in the end (Carol here) to view its stats instead of Bob's"
}

git status | grep -q lists/
if test $? -eq 0
then
    echo "lists/ directory not clean. commit your changes" >&2
    exit 1
fi

if test "$2" != "is"
then
    usage
    exit 2
fi

if test "$4" != "for" -a "$4" != ""
then
    usage
    exit 2
fi

alice="$1"
bob="$3"
Alice="${alice^}"
Bob="${bob^}"
carol="$5"

for file in lists/"$alice"-*
do
    sed -i "s/# $Alice/# $Bob/" "$file"
    mv "$file" "lists/$bob-${file#*-}"
done

if test "" = "$carol"
then
    python stats.py $bob
else
    python stats.py $carol
fi

rm lists/"$bob"-*
git restore lists/
