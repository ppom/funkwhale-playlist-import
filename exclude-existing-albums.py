#!/usr/bin/env python3

"""
From a list of albums intended to be imported into Funkwhale,
print the list of albums which are not already in the federation.
"""

import argparse
import fileinput
import logging
import sys

from textwrap import dedent

import deezer

import utils
from funkwhale_api import FunkwhaleAPI, FunkwhaleMatcher

MIN_THRESHOLD = 0
MAX_THRESHOLD = 100


class CustomFormatter(
    argparse.ArgumentDefaultsHelpFormatter, argparse.RawTextHelpFormatter
):
    pass


def build_parser() -> argparse.ArgumentParser:
    """Returns a parser for the script with help built in."""
    parser = argparse.ArgumentParser(
        description="filter out and print non-existing albums on a Funkwhale instance from a list",
        epilog=dedent(
            """
        list should be formatted with one item per line.

        For streaming services, use the public link.
        If submitting a textual list, use the following formatting :
        
        Title1 - Album1
        Title2 - Album2
        """
        ),
        formatter_class=CustomFormatter,
    )
    parser.add_argument(
        "path",
        # Optional positional argument: either specified or default stdin so playlist can be piped into this program
        nargs="?",
        help="path of the file containing the text-formatted albums.\nuse - for stdin",
        default="-",
    )
    parser.add_argument(
        "-t",
        "--threshold",
        default=20,
        help=f"maximal distance {MIN_THRESHOLD} <= N <= {MAX_THRESHOLD} for two strings to be considered similar",
    )
    parser.add_argument(
        "-s",
        "--source",
        default="text",
        # FIXME add Spotify ?
        choices=["text", "deezer"],
        help="indicates the source of albums, either text or links to streaming services",
    )
    parser.add_argument(
        "--try-hard",
        action="store_true",
        help="split terms of search to maximize chances of findings in special cases",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="INFO",
        # Trick to convert string to numerical log level
        type=lambda x: getattr(logging, x.upper()),
        help="configure the logging level (DEBUG, INFO, WARNING, or ERROR)",
    )
    return parser


def albums_from_deezer(links: list[str]) -> list[utils.ArtistItem]:
    """
    Extract albums from deezer links.

    We use a library because the Deezer API needs authorization,
    is really complex and parsing HTML is too fragile.
    """
    albums: list[utils.ArtistItem] = []
    client = deezer.Client()
    for link in links:
        print(link)
        # ID is the last part of the URL
        album_id = link.split("/")[-1]
        logging.debug(f"Getting album {album_id}...")
        album = client.get_album(album_id)
        albums.append(utils.ArtistItem(album.artist.name, album.title))
    return albums


def main():
    """Main logic to deduplicate albums."""
    # Get and validate arguments from the CLI
    parser = build_parser()
    args = parser.parse_args()
    if not 0 <= args.threshold <= 100:
        parser.print_help()
        raise ValueError(
            f"Threshold should be between {MIN_THRESHOLD} and {MAX_THRESHOLD}."
        )

    # Configure logging
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s:%(levelname)-7s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(args.log_level)

    # Authenticate to the instance and configure utility matcher
    instance_url = utils.get_instance_url()
    token = utils.get_token()
    api = FunkwhaleAPI(instance_url, token)
    matcher = FunkwhaleMatcher(api, args.threshold, args.try_hard)
    user_id = api.get_me()["id"]

    # Extract original list
    source_albums: list[str] = []
    with fileinput.input(files=args.path) as f:
        for line in f:
            source_albums.append(line.strip())

    # Extract information from source
    try:
        if args.source == "text":
            logging.debug(f"Parsing text file {args.path}...")
            albums = utils.read_txt_artist_item_content(source_albums)
        if args.source == "deezer":
            logging.debug(f"Getting album information...")
            albums = albums_from_deezer(source_albums)
    except utils.LocalError:
        logging.exception(f"Error when parsing file {args.path}, aborting.")
        sys.exit(1)
    
    matcher = FunkwhaleMatcher(api, args.threshold, args.try_hard)
    existing_index = []
    for index, album in enumerate(albums):
        album_matches = matcher.matching_item(album, utils.FunkwhaleArtistItem.ALBUM)
        if album_matches:
            logging.debug(f"Found matches {album_matches} for album {album}")
            existing_index.append(index)

    unique_albums = set()
    for index, album in enumerate(source_albums):
        if index not in existing_index:
            unique_albums.add(source_albums[index])

    for album in unique_albums:
        print(album)

if __name__ == "__main__":
    main()
