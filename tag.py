#!/usr/bin/env python
"""
Heavily inspired from https://github.com/beetbox/beets/tree/master/beetsplug/lastgenre
Goal is to add **generic genres** as tags to existing Funkwhale albums, e.g. pop, rock, no
obscure genres which are not very useful. 

The scripts works with a whitelist (all genres that you want to consider) and a canonicalization tree (see data folder).
It fetches top genres from Last.FM and keeps the "closest parent genres" of the canonicalization tree.
If Last.FM does not return results (which is bizarrely common for topTags requests, even if album is tagged),
the script will try to canonicalize existing Funkwhale tags.

Then it adds the new found tags to the old ones.

The script should be idempotent.
"""

import logging
import os
import yaml
import re

import pylast

from funkwhale_api import FunkwhaleAPI, FunkwhaleAPIBadStatusCode
import utils


def deduplicate(seq):
    """Remove duplicates from sequence wile preserving order."""
    seen = set()
    return [x for x in seq if x not in seen and not seen.add(x)]


def flatten_tree(elem, path, branches):
    """Flatten nested lists/dictionaries into lists of strings
    (branches).
    """
    if not path:
        path = []

    if isinstance(elem, dict):
        for k, v in elem.items():
            flatten_tree(v, path + [k], branches)
    elif isinstance(elem, list):
        for sub in elem:
            flatten_tree(sub, path, branches)
    else:
        branches.append(path + [str(elem)])


def find_parents(candidate, branches):
    """Find parents genre of a given genre, ordered from the closest to
    the further parent.
    """
    for branch in branches:
        try:
            idx = branch.index(candidate.lower())
            return list(reversed(branch[: idx + 1]))
        except ValueError:
            continue
    return [candidate]


instance_url = utils.get_instance_url()
token = utils.get_token()
lastfm_key = utils.get_lastfm_key()
lastfm_secret = utils.get_lastfm_secret()
lastfm = pylast.LastFMNetwork(api_key=lastfm_key, api_secret=lastfm_secret)

whitelist_filename = os.path.join(os.path.dirname(__file__), "data/genres.txt")
genres_tree_filemame = os.path.join(os.path.dirname(__file__), "data/genres-tree.yaml")
genres_branches = []
with open(genres_tree_filemame, "r", encoding="utf-8") as f:
    genres_tree = yaml.safe_load(f)
    flatten_tree(genres_tree, [], genres_branches)

whitelist = set()
with open(whitelist_filename, "r") as f:
    for line in f:
        if not line.startswith("#"):
            whitelist.add(line.strip().lower())

fw = FunkwhaleAPI(instance_url, token)
all_albums = []
albums_page_ctr = 1
album_continue_fetch = True
while album_continue_fetch:
    try:
        # Maximum page_size is 50
        params_albums = {"page": albums_page_ctr, "page_size": 50, "scope": "me"}
        all_albums += fw.get_albums(params_albums)['results']
        albums_page_ctr += 1
    except FunkwhaleAPIBadStatusCode as e:
        album_continue_fetch = False

for album in all_albums:
    try:
        title = album["title"]
        artist = album["artist"]["name"]
        tags = album["tags"]
        album_lastfm = lastfm.get_album(artist, title)
        
        # We get all tags on Last.fm, which can be anything from genre to year of release
        genres = album_lastfm.get_top_tags()
        if not genres:
            # Get artist genres instead of album
            artist_lastfm = lastfm.get_artist(artist)
            genres = artist_lastfm.get_top_tags()

        genre_names = [el.item.get_name().lower() for el in genres]


        # We add existing genres found on the instance
        # Added at the end so not prioritary
        for tag in tags:
            tag_space = ' '.join(re.findall('[A-Z][^A-Z]*', tag)).lower()
            genre_names.append(tag_space)

        tags_all = []
        for tag in genre_names:
            # And we get the whitelisted parents, closest to further
            # The idea is to keep the whitelist of reasonable size so
            # tags are useful in search, radios, etc.
            tags_all += [
                # Title case for all tags
                x.title().replace(' ', '')
                for x in find_parents(tag, genres_branches)
                if x is not None and x in whitelist
            ]
        tags_all = deduplicate(tags_all)[:1]
        if diffs :=  set(tags_all) - set(tags):
            print(f'Found new tags for {title} by {artist}: {diffs}')
            # Merge with existing Funkwhale tags
            tags_all += tags
            tags_all = deduplicate(tags_all)
            fw.replace_album_tags(album['id'], tags_all)
    except pylast.WSError as e:
        pass
    except Exception as e:
        print(e)
        pass