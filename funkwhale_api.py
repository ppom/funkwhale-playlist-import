"""
Helper classes to communicate with Funwkhale v1 API.
"""
import logging

from functools import cache
from posixpath import join
from typing import Dict

from requests import Session, Response
from rapidfuzz import process, fuzz

from utils import RemoteError, FunkwhaleArtistItem, ArtistItem


class FunkwhaleAPIError(RemoteError):
    """Base exception class for Funkwhale API errors."""


class FunkwhaleAPIBadStatusCode(FunkwhaleAPIError):
    """
    Exception used to indicate that a API call has
    not succeeded because of an unknown error.
    """

    def __init__(self, message: str, response: Response):
        """
        response: original response from the funkwhale API
        """
        super().__init__(message)
        self.response = response


class FunkwhaleAPIRateLimit(FunkwhaleAPIError):
    """
    Exception used to indicate that a API call has
    been rejected because of an instance rate limit.
    """

    def __init__(self, message: str, retry_after: int):
        """
        retry_after: delay in seconds before another call can be made.
        """
        super().__init__(message)
        self.retry_after = retry_after


class FunkwhaleAPISession(Session):
    """
    Funkwhale API Session Handler.
    Wrapper around Session to set the base API URL
    and throw an exception on unexpected return code.
    """

    CORRECT_STATUS_CODE = {"GET": 200, "PATCH": 200, "POST": 201, "DELETE": 204}

    def __init__(self, instance_url: str, *args, **kwargs):
        """
        Init the parent `requests.session` object and
        store the funkwhale instance URL.
        """
        super().__init__(*args, **kwargs)
        self._instance_url = instance_url

    def request(self, method, url, *args, **kwargs):
        """
        Call the API with the configured session.

        url is to be understood as an endpoint of the API.
        """
        url = join(self._instance_url, "api/v1", url)
        response = super().request(method, url, *args, **kwargs)
        if self.CORRECT_STATUS_CODE[method] != response.status_code:
            raise FunkwhaleAPIBadStatusCode(
                f"Bad return code {response.status_code} for request {method} on endpoint {url}",
                response,
            )
        if response.status_code == 429:
            raise FunkwhaleAPIRateLimit(
                f"Request {method} on endpoint {url} has been dropped because of rate limit",
                response.headers["Retry-After"],
            )
        return response


class FunkwhaleAPI:
    """
    Simple wrapper around the subset of the Funkwhale API
    that we use in the other scripts.

    It makes code more readable by hiding requests calls and
    API endpoints.

    It also throws exceptions on unexpected HTTP status codes.

    Each instance is bound to an authenticated session.
    """

    def __init__(self, instance_url: str, auth_token: str):
        """
        Authenticate to a funkwhale instance through OAuth2.

        auth_token must by generated as follows : https://docs.funkwhale.audio/develop/developers/authentication.html#oauth
        """
        self._session = FunkwhaleAPISession(instance_url)
        self._session.headers.update({"Authorization": f"Bearer {auth_token}"})

    def get_me(self) -> Dict:
        """Get current user information."""
        return self._session.get("users/me").json()

    def get_artists(self, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Library%20and%20metadata/get_api_v1_artists_
        """
        return self._session.get("artists", params=params).json()

    def get_tracks(self, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Library%20and%20metadata/get_api_v1_tracks_
        """
        return self._session.get("tracks", params=params).json()

    def get_track(self, track_id: int, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Library%20and%20metadata/get_api_v1_tracks__id__
        """
        return self._session.get(f"tracks/{track_id}", params=params).json()

    def get_albums(self, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Library%20and%20metadata/get_api_v1_albums_
        """
        return self._session.get("albums", params=params).json()

    def create_playlist(self, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Content%20curation/post_api_v1_playlists_
        """
        return self._session.post("playlists", data=params).json()

    def get_playlists(self, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Content%20curation/get_api_v1_playlists_
        """
        return self._session.get("playlists", params=params).json()

    def get_playlist_tracks(self, playlist_id: int) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Content%20curation/get_api_v1_playlists__id__tracks
        """
        return self._session.get(f"playlists/{playlist_id}").json()

    def add_tracks_to_playlist(self, playlist_id: int, params: Dict = None) -> Dict:
        """
        See https://docs.funkwhale.audio/swagger/#/Content%20curation/post_api_v1_playlists__id__add

        playlist_id is returned when creating the playlist so
        adding tracks can be automated after creation.
        """
        return self._session.post(f"playlists/{playlist_id}/add", data=params).json()

    def clear_playlist(self, playlist_id: int):
        """
        See https://docs.funkwhale.audio/swagger/#/Content%20curation/delete_api_v1_playlists__id__clear
        """
        self._session.delete(f"playlists/{playlist_id}/clear")

    def replace_album_tags(self, album_id, tags):
        """Not documented."""
        body = {
            "is_approved": True,
            "payload": {"tags": tags},
            "summary": "",
            "type": "update",
        }
        return self._session.post(f"albums/{album_id}/mutations", json=body).json()


class FunkwhaleMatcher:
    """
    Utility methods to get matching entities from text to a Funkwhale instance, using similarity.

    Results are cached to avoid intensive API calls and computations.
    """

    def __init__(self, api: FunkwhaleAPI, threshold: int, split: bool):
        """
        Records :
        * The Funkwhale instance used for lookup
        * The similiary threshold to considerer whetever an entity matches or not
        * The split option, which instructs to search term by term instead of all terms at once
        """
        self._api = api
        self._threshold = threshold
        self._split = split

    @cache
    def matching_artists(self, artist: str) -> set[str]:
        """
        From a name, get unique ids of all matching artists
        on the Funkwhale instance whose similarity is below the threshold.
        """
        lookup_artists = set(artist.split(" ")) if self._split else {artist}
        # FIXME Funkwhale still seams to have a hard time for artist with slash,
        # this workaround is equivalent to --try-hard for slashes.
        lookup_artists = set(
            [
                # FIXME still a dirty fix for titles or artists with parenthesis,
                # the search is broken
                artist_slash.replace('(', ' ').replace(')', '')
                for artist_space in lookup_artists
                for artist_slash in artist_space.split("/")
            ]
        )
        logging.debug(f"Artist search terms are {lookup_artists}")
        candidate_artists = {
            x["id"]: x["name"]
            for lookup in lookup_artists
            for x in self._api.get_artists({"q": lookup})["results"]
        }
        logging.debug(
            f"Candidates artists on Funkwhale are: {[v for (k, v) in candidate_artists.items()]}"
        )
        sorted_artists = process.extract(
            artist,
            candidate_artists,
            scorer=fuzz.WRatio,
            score_cutoff=self._threshold,
        )
        # process.extract, in that case, returns (artist name, distance, artist id)
        # See https://maxbachmann.github.io/RapidFuzz/Usage/process.html#extract
        logging.debug(f"Matching artists are: { {x[0] for x in sorted_artists} }")
        return {x[2] for x in sorted_artists}

    @cache
    def matching_item(
        self,
        item: ArtistItem,
        item_type: FunkwhaleArtistItem = FunkwhaleArtistItem.TRACK,
    ) -> list[str]:
        """
        From a artist item title and an optional artist, get ids of all matching
        item on the Funkwhale instance, whose similirary is below the
        threshold, ordered from descendant similarity .
        """
        # See matching_artists
        lookup_items = set(item.title.split(" ")) if self._split else {item.title.replace('(', ' ').replace(')', '')}
        logging.debug(f"Track search terms are {lookup_items}")
        api_method = (
            self._api.get_tracks
            if item_type == FunkwhaleArtistItem.TRACK
            else self._api.get_albums
        )
        if not item.artist:
            candidate_items = {
                x["id"]: x["title"]
                for lookup in lookup_items
                for x in api_method({"q": lookup})["results"]
            }
        else:
            candidate_items = {
                x["id"]: x["title"]
                for lookup in lookup_items
                # Will produce empty candidates set if set of found artists is empty
                for id in self.matching_artists(item.artist)
                for x in api_method({"q": lookup, "artist": id})["results"]
            }
        logging.debug(
            f"Candidates titles on Funkwhale are: {[v for (k, v) in candidate_items.items()]}"
        )
        sorted_titles = process.extract(
            item.title,
            candidate_items,
            scorer=fuzz.WRatio,
            score_cutoff=self._threshold,
        )
        logging.debug(f"Matching titles are: { [x[0] for x in sorted_titles] }")
        return [x[2] for x in sorted_titles]
